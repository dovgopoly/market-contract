const GoToken = artifacts.require('GoToken');

module.exports = function(deployer, network, [owner, ...accounts]) {
  switch (network) {
    case "test":
      break;
    case "ganache":
      deployer.deploy(GoToken, {from: owner});
      break;
    default:
      assert(false);
  }
};
