package main

import (
	"context"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/kr/pretty"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/dovgopoly/market-contract/internal/eth"
	"log"
	"math/big"
)

var (
	cfg struct {
		URL           string `mapstructure:"url"`
		OwnerPvk      string `mapstructure:"owner-pvk"`
		ClientPvk     string `mapstructure:"client-pvk"`
		TokenAddress  string `mapstructure:"token-address"`
		MarketAddress string `mapstructure:"market-address,omitempty"`
	}
	client *ethclient.Client
	token  *eth.TokenContract
	market *eth.MarketContract
)

const (
	marketName        = "GO MARKET"
	ethAmountToBuy    = "50000000000000000"
	tokenAmountToSell = ethAmountToBuy + "000"
)

func init() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	var err error
	if err = viper.ReadInConfig(); err != nil {
		log.Fatal(errors.Wrap(err, "failed to read cfg"))
	}
	if err = viper.Unmarshal(&cfg); err != nil {
		log.Fatal(errors.Wrap(err, "failed to unmarshal cfg"))
	}
	log.Printf("config has been read: %#v\n", cfg)
}

func printClientBalances(stream pretty.Printfer, message string) error {
	address, err := eth.AddressFromPrivateKey(cfg.ClientPvk)
	if err != nil {
		return errors.Wrap(err, "failed to get client address")
	}
	ethBalance, err := client.BalanceAt(context.Background(), address, nil)
	if err != nil {
		return errors.Wrap(err, "failed to get ethereum balance")
	}
	tokenBalance, err := token.BalanceOf(address)
	if err != nil {
		return errors.Wrap(err, "failed to get token balance")
	}
	stream.Printf("%s (token balance; eth balance) = (%s; %s)\n",
		message, tokenBalance.String(), ethBalance.String())

	return nil
}

func deployMarket(token *eth.TokenContract) error {
	ownerAddress, err := eth.AddressFromPrivateKey(cfg.OwnerPvk)
	if err != nil {
		return errors.Wrap(err, "failed to get owner address")
	}

	ownerBalance, err := token.BalanceOf(ownerAddress)
	if err != nil {
		return errors.Wrap(err, "failed to get owner balance")
	}

	if err = token.AddMarket(marketName, ownerBalance, cfg.OwnerPvk); err != nil {
		return errors.Wrap(err, "failed to add market")
	}

	if cfg.MarketAddress, err = token.GetMarketAddress(marketName); err != nil {
		return errors.Wrap(err, "failed to get market address")
	}

	return nil
}

func getContracts() error {
	var err error
	token, err = eth.NewTokenContract(cfg.TokenAddress, client)
	if err != nil {
		return errors.Wrap(err, "failed to create token contract")
	}

	cfg.MarketAddress, err = token.GetMarketAddress(marketName)
	if err != nil {
		return errors.Wrap(err, "failed to get market address")
	}

	if eth.IsZeroAddress(cfg.MarketAddress) {
		if err = deployMarket(token); err != nil {
			return errors.Wrap(err, "failed to deploy market")
		}
	}

	market, err = token.GetMarket(marketName)
	if err != nil {
		return errors.Wrap(err, "failed to get market")
	}

	return nil
}

func run() error {
	var err error

	client, err = ethclient.Dial(cfg.URL)
	if err != nil {
		return errors.Wrap(err, "failed to connect")
	}

	if err = getContracts(); err != nil {
		return errors.Wrap(err, "failed to get contracts")
	}

	amountToBuy, ok := new(big.Int).SetString(ethAmountToBuy, 10)
	if !ok {
		return errors.New("failed to create buy amount")
	}

	if err = printClientBalances(log.Default(), "Before buy"); err != nil {
		return errors.Wrap(err, "failed to print before buy balance")
	}

	if err := market.Buy(amountToBuy, cfg.ClientPvk); err != nil {
		return errors.Wrap(err, "failed to buy")
	}

	if err = printClientBalances(log.Default(), "After buy"); err != nil {
		return errors.Wrap(err, "failed to print before buy balance")
	}

	amountToSell, ok := new(big.Int).SetString(tokenAmountToSell, 10)
	if !ok {
		return errors.New("failed to create sell amount")
	}

	if err := token.Approve(common.HexToAddress(cfg.MarketAddress), amountToSell, cfg.ClientPvk); err != nil {
		return errors.Wrap(err, "failed to approve")
	}

	if err := market.Sell(amountToSell, cfg.ClientPvk); err != nil {
		return errors.Wrap(err, "failed to sell")
	}

	if err = printClientBalances(log.Default(), "After sell"); err != nil {
		return errors.Wrap(err, "failed to print before buy balance")
	}

	return nil
}

func main() {
	if err := run(); err != nil {
		panic(err)
	}
}
