const GoToken = artifacts.require('GoToken');

const { constants, expectRevert } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

contract('GoToken', function ([owner, ...accounts]) {
    const marketName = 'market';
    const tokenName = 'GoToken';
    const tokenSymbol = 'GT';

    beforeEach(async () => {
        this.token = await GoToken.new({from: owner});
    });

    it('should deploy properly', async () => {
        const expectedAmount = web3.utils.toWei('1000', 'ether');
        expect(await this.token.balanceOf(owner)).to.be.bignumber.equal(expectedAmount);
        expect(await this.token.couldBeMinted()).to.be.bignumber.equal(expectedAmount);
        expect(await this.token.name()).equal(tokenName);
        expect(await this.token.symbol()).equal(tokenSymbol);
    });

    describe('when market added', async () => {
        const validMarketName = 'valid-market';

        beforeEach(async () => {
           await this.token.addMarket(marketName, await this.token.balanceOf(owner), {from: owner});
        });

        it('should add market properly', async () => {
            expect(await this.token.markets(marketName)).not.equal(constants.ZERO_ADDRESS);
            expect(await this.token.balanceOf(owner)).to.be.bignumber.equal('0');
        });

        it('should not add market with same name', async () => {
           await expectRevert(
               this.token.addMarket(marketName, 0, {from: owner}),
               'Market with such name already exists'
           );
        });

        it('should not add market when balance is less than init one', async () => {
            await expectRevert(
                this.token.addMarket(validMarketName, 1, {from: owner}),
                'Your current balance is less than proposed init balance'
            );
        });

        it('should not allow add market from non-owner account', async () => {
            await expectRevert(
                this.token.addMarket(validMarketName, 0, {from: accounts[1]}),
                'Ownable: caller is not the owner'
            );
        });

        it('should add one more market if conditions are met', async () => {
            await this.token.addMarket(validMarketName, 0, {from: owner});
            const validMarketAddress = await this.token.markets(validMarketName);
            expect(validMarketAddress).not.equal(await this.token.markets(marketName));
            expect(validMarketAddress).not.equal(constants.ZERO_ADDRESS);
        });
    });
});
