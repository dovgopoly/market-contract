const GoToken = artifacts.require('GoToken');
const GoMarket = artifacts.require('GoMarket');

const { BN, constants, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');

contract('GoMarket', function ([owner, customer, ...accounts]) {
    const marketName = 'market';

    beforeEach(async () => {
        this.token = await GoToken.new({from: owner});
        const ownerBalance = await this.token.balanceOf(owner);
        await this.token.addMarket(marketName, ownerBalance, {from: owner});
        const marketAddress = await this.token.markets(marketName);
        this.market = await GoMarket.at(marketAddress);
    });

    it('should deploy properly', async () => {
        expect(this.market.address).not.equal(constants.ZERO_ADDRESS);
        expect(this.market.address).equal(await token.markets(marketName));
        expect(await this.market.balance()).to.be.bignumber.equal(await this.token.couldBeMinted());
    });

    it('should buy tokens if all conditions are met', async () => {
        const ethToPay = 1;
        const tokensAmount = new BN(ethToPay * (await this.market.tokensPerEth()));

        const tx = await this.market.buy({from: customer, value: ethToPay});
        expectEvent(tx, 'Bought', {buyer: customer, amount: tokensAmount});
        expect(await this.token.balanceOf(customer)).to.be.bignumber.equal(tokensAmount);
    });

    it('should not buy 0 tokens', async () => {
        await expectRevert(
            this.market.buy({from: customer, value: 0}),
            "Cannot buy 0 tokens"
        );
    });

    it('should not buy if not enough tokens to transfer', async () => {
        await expectRevert(
            this.market.buy({from: customer, value: web3.utils.toWei('2', 'ether')}),
            'Market does not have enough tokens'
        );
    });

    describe('when customer bought tokens',  () => {
        const tokensAmountToBuy = new BN(web3.utils.toWei('1', 'ether'));
        const tokensCost = new BN(web3.utils.toWei('1', 'milliether'));

        beforeEach(async () => {
           await this.market.buy({from: customer, value: tokensCost});
           expect(await this.token.balanceOf(customer)).to.be.bignumber.equal(tokensAmountToBuy);
        });

        it('should withdraw all eth', async () => {
            const balanceBefore = await web3.eth.getBalance(this.market.address);
            expect(balanceBefore).not.equal('0');
            await this.market.withdraw({from: owner});
            expect(await web3.eth.getBalance(this.market.address)).equal('0');
        });

        it('should not sell if allowance is not provided', async () => {
            await expectRevert(
                this.market.sell(tokensAmountToBuy, {from: customer}),
                'Not enough allowance to transfer tokens'
            );
        });

        describe('when allowance is provided', async () => {
            beforeEach(async () => {
                await this.token.approve(this.market.address, tokensAmountToBuy, {from: customer});
            })

            it('should not sell if contract does not have enough eth', async () => {
                await this.market.withdraw({from: owner});
                await expectRevert(
                    this.market.sell(tokensAmountToBuy, {from: customer}),
                    'Contract does not have enough eth'
                );
            });

            it('should not sell if customer does not have enough tokens', async () => {
                await this.token.transfer(owner, await this.token.balanceOf(customer), {from: customer});

                await expectRevert(
                    this.market.sell(1, {from: customer}),
                    'Not enough tokens to sell'
                );
            });

            it('should sell if all conditions are met', async () => {
                const tx = await this.market.sell(tokensAmountToBuy, {from: customer});
                expectEvent(tx, 'Sold', {seller: customer, amount: tokensAmountToBuy});
                expect(await this.token.balanceOf(customer)).to.be.bignumber.equal('0');
            });
        });
    });
});
