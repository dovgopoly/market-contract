// SPDX-License-Identifier: MIT

pragma solidity ^0.8.13;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./GoToken.sol";

contract GoMarket is Ownable {
    event Bought(address indexed buyer, uint amount);
    event Sold(address indexed seller, uint amount);

    uint public tokensPerEth = 1000;
    GoToken public token;

    constructor(address tokenAddr) {
        token = GoToken(tokenAddr);
    }

    function setExchangeRate(uint newTokensPerEth) external onlyOwner {
        tokensPerEth = newTokensPerEth;
    }

    function buy() external payable returns(bool) {
        require(msg.value > 0, "Cannot buy 0 tokens");

        uint tokensToBuy = msg.value * tokensPerEth;

        require(
            token.balanceOf(address(this)) >= tokensToBuy,
            "Market does not have enough tokens"
        );
        require(
            token.transfer(msg.sender, tokensToBuy),
            "Error occurred while transferring tokens"
        );

        emit Bought(msg.sender, tokensToBuy);

        return true;
    }

    function sell(uint _tokensToSell) external returns(bool) {
        require(_tokensToSell >= tokensPerEth, "Not enough tokens to sell");

        uint tokensToSell = _tokensToSell - _tokensToSell % tokensPerEth;
        uint ethToTransfer = tokensToSell / tokensPerEth;

        require(
            address(this).balance >= ethToTransfer,
            "Contract does not have enough eth"
        );
        require(
            token.balanceOf(msg.sender) >= tokensToSell,
            "Not enough tokens to sell"
        );
        require(
            token.allowance(msg.sender, address(this)) >= tokensToSell,
            "Not enough allowance to transfer tokens"
        );
        require(
            token.transferFrom(msg.sender, address(this), tokensToSell),
            "Error occurred while transferring tokens"
        );

        (bool ok,) = msg.sender.call{value: ethToTransfer}("");
        require(ok, "Error occurred while transferring eth");

        emit Sold(msg.sender, tokensToSell);

        return true;
    }

    function balance() external view returns(uint) {
        return token.balanceOf(address(this));
    }

    function withdraw() external onlyOwner returns(bool) {
        require(address(this).balance > 0, "No funds to withdraw");

        (bool ok,) = msg.sender.call{value: address(this).balance}("");
        require(ok, "Error occurred while transferring eth");

        return true;
    }
}
