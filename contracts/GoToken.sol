// SPDX-License-Identifier: MIT

pragma solidity ^0.8.13;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./GoMarket.sol";

contract GoToken is ERC20, Ownable {
    uint public couldBeMinted = 1000 * 10 ** 18;

    mapping(string => GoMarket) public markets;

    constructor() ERC20("GoToken", "GT") {
        _mint(msg.sender, couldBeMinted);
    }

    function addMarket(
        string memory name,
        uint initBalance
    )
        external
        onlyOwner
    {
        require(
            balanceOf(msg.sender) >= initBalance,
            "Your current balance is less than proposed init balance"
        );
        require(
            address(markets[name]) == address(0),
            "Market with such name already exists"
        );

        markets[name] = new GoMarket(address(this));
        markets[name].transferOwnership(owner());
        transfer(address(markets[name]), initBalance);
    }
}
