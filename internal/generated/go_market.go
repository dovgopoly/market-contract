// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package generated

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// MarketMetaData contains all meta data concerning the Market contract.
var MarketMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"tokenAddr\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"buyer\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Bought\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"previousOwner\",\"type\":\"address\"},{\"indexed\":true,\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"OwnershipTransferred\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":true,\"internalType\":\"address\",\"name\":\"seller\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"Sold\",\"type\":\"event\"},{\"inputs\":[],\"name\":\"balance\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"buy\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"payable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"owner\",\"outputs\":[{\"internalType\":\"address\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"renounceOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"_tokensToSell\",\"type\":\"uint256\"}],\"name\":\"sell\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"uint256\",\"name\":\"newTokensPerEth\",\"type\":\"uint256\"}],\"name\":\"setExchangeRate\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"token\",\"outputs\":[{\"internalType\":\"contractGoToken\",\"name\":\"\",\"type\":\"address\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"tokensPerEth\",\"outputs\":[{\"internalType\":\"uint256\",\"name\":\"\",\"type\":\"uint256\"}],\"stateMutability\":\"view\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"newOwner\",\"type\":\"address\"}],\"name\":\"transferOwnership\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[],\"name\":\"withdraw\",\"outputs\":[{\"internalType\":\"bool\",\"name\":\"\",\"type\":\"bool\"}],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x60806040526103e86001553480156200001757600080fd5b5060405162001a6538038062001a6583398181016040528101906200003d9190620001db565b6200005d62000051620000a560201b60201c565b620000ad60201b60201c565b80600260006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550506200020d565b600033905090565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050816000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055508173ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a35050565b600080fd5b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000620001a38262000176565b9050919050565b620001b58162000196565b8114620001c157600080fd5b50565b600081519050620001d581620001aa565b92915050565b600060208284031215620001f457620001f362000171565b5b60006200020484828501620001c4565b91505092915050565b611848806200021d6000396000f3fe6080604052600436106100915760003560e01c8063cbdd69b511610059578063cbdd69b51461014c578063db068e0e14610177578063e4849b32146101a0578063f2fde38b146101dd578063fc0c546a1461020657610091565b80633ccfd60b14610096578063715018a6146100c15780638da5cb5b146100d8578063a6f2ae3a14610103578063b69ef8a814610121575b600080fd5b3480156100a257600080fd5b506100ab610231565b6040516100b89190610e51565b60405180910390f35b3480156100cd57600080fd5b506100d66103a6565b005b3480156100e457600080fd5b506100ed61042e565b6040516100fa9190610ead565b60405180910390f35b61010b610457565b6040516101189190610e51565b60405180910390f35b34801561012d57600080fd5b506101366106c0565b6040516101439190610ee1565b60405180910390f35b34801561015857600080fd5b50610161610763565b60405161016e9190610ee1565b60405180910390f35b34801561018357600080fd5b5061019e60048036038101906101999190610f2d565b610769565b005b3480156101ac57600080fd5b506101c760048036038101906101c29190610f2d565b6107ef565b6040516101d49190610e51565b60405180910390f35b3480156101e957600080fd5b5061020460048036038101906101ff9190610f86565b610c4d565b005b34801561021257600080fd5b5061021b610d44565b6040516102289190611012565b60405180910390f35b600061023b610d6a565b73ffffffffffffffffffffffffffffffffffffffff1661025961042e565b73ffffffffffffffffffffffffffffffffffffffff16146102af576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016102a69061108a565b60405180910390fd5b600047116102f2576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016102e9906110f6565b60405180910390fd5b60003373ffffffffffffffffffffffffffffffffffffffff164760405161031890611147565b60006040518083038185875af1925050503d8060008114610355576040519150601f19603f3d011682016040523d82523d6000602084013e61035a565b606091505b505090508061039e576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610395906111ce565b60405180910390fd5b600191505090565b6103ae610d6a565b73ffffffffffffffffffffffffffffffffffffffff166103cc61042e565b73ffffffffffffffffffffffffffffffffffffffff1614610422576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016104199061108a565b60405180910390fd5b61042c6000610d72565b565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16905090565b600080341161049b576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016104929061123a565b60405180910390fd5b6000600154346104ab9190611289565b905080600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166370a08231306040518263ffffffff1660e01b81526004016105099190610ead565b602060405180830381865afa158015610526573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061054a91906112f8565b101561058b576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161058290611397565b60405180910390fd5b600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1663a9059cbb33836040518363ffffffff1660e01b81526004016105e89291906113b7565b6020604051808303816000875af1158015610607573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061062b919061140c565b61066a576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610661906114ab565b60405180910390fd5b3373ffffffffffffffffffffffffffffffffffffffff167fc55650ccda1011e1cdc769b1fbf546ebb8c97800b6072b49e06cd560305b1d67826040516106b09190610ee1565b60405180910390a2600191505090565b6000600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166370a08231306040518263ffffffff1660e01b815260040161071d9190610ead565b602060405180830381865afa15801561073a573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061075e91906112f8565b905090565b60015481565b610771610d6a565b73ffffffffffffffffffffffffffffffffffffffff1661078f61042e565b73ffffffffffffffffffffffffffffffffffffffff16146107e5576040517f08c379a00000000000000000000000000000000000000000000000000000000081526004016107dc9061108a565b60405180910390fd5b8060018190555050565b6000600154821015610836576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161082d90611517565b60405180910390fd5b6000600154836108469190611566565b836108519190611597565b905060006001548261086391906115cb565b9050804710156108a8576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161089f9061166e565b60405180910390fd5b81600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166370a08231336040518263ffffffff1660e01b81526004016109049190610ead565b602060405180830381865afa158015610921573d6000803e3d6000fd5b505050506040513d601f19601f8201168201806040525081019061094591906112f8565b1015610986576040517f08c379a000000000000000000000000000000000000000000000000000000000815260040161097d90611517565b60405180910390fd5b81600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1663dd62ed3e33306040518363ffffffff1660e01b81526004016109e492919061168e565b602060405180830381865afa158015610a01573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610a2591906112f8565b1015610a66576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610a5d90611729565b60405180910390fd5b600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff166323b872dd3330856040518463ffffffff1660e01b8152600401610ac593929190611749565b6020604051808303816000875af1158015610ae4573d6000803e3d6000fd5b505050506040513d601f19601f82011682018060405250810190610b08919061140c565b610b47576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610b3e906114ab565b60405180910390fd5b60003373ffffffffffffffffffffffffffffffffffffffff1682604051610b6d90611147565b60006040518083038185875af1925050503d8060008114610baa576040519150601f19603f3d011682016040523d82523d6000602084013e610baf565b606091505b5050905080610bf3576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610bea906111ce565b60405180910390fd5b3373ffffffffffffffffffffffffffffffffffffffff167fae92ab4b6f8f401ead768d3273e6bb937a13e39827d19c6376e8fd4512a05d9a84604051610c399190610ee1565b60405180910390a260019350505050919050565b610c55610d6a565b73ffffffffffffffffffffffffffffffffffffffff16610c7361042e565b73ffffffffffffffffffffffffffffffffffffffff1614610cc9576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610cc09061108a565b60405180910390fd5b600073ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff1603610d38576040517f08c379a0000000000000000000000000000000000000000000000000000000008152600401610d2f906117f2565b60405180910390fd5b610d4181610d72565b50565b600260009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b600033905090565b60008060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050816000806101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff1602179055508173ffffffffffffffffffffffffffffffffffffffff168173ffffffffffffffffffffffffffffffffffffffff167f8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e060405160405180910390a35050565b60008115159050919050565b610e4b81610e36565b82525050565b6000602082019050610e666000830184610e42565b92915050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b6000610e9782610e6c565b9050919050565b610ea781610e8c565b82525050565b6000602082019050610ec26000830184610e9e565b92915050565b6000819050919050565b610edb81610ec8565b82525050565b6000602082019050610ef66000830184610ed2565b92915050565b600080fd5b610f0a81610ec8565b8114610f1557600080fd5b50565b600081359050610f2781610f01565b92915050565b600060208284031215610f4357610f42610efc565b5b6000610f5184828501610f18565b91505092915050565b610f6381610e8c565b8114610f6e57600080fd5b50565b600081359050610f8081610f5a565b92915050565b600060208284031215610f9c57610f9b610efc565b5b6000610faa84828501610f71565b91505092915050565b6000819050919050565b6000610fd8610fd3610fce84610e6c565b610fb3565b610e6c565b9050919050565b6000610fea82610fbd565b9050919050565b6000610ffc82610fdf565b9050919050565b61100c81610ff1565b82525050565b60006020820190506110276000830184611003565b92915050565b600082825260208201905092915050565b7f4f776e61626c653a2063616c6c6572206973206e6f7420746865206f776e6572600082015250565b600061107460208361102d565b915061107f8261103e565b602082019050919050565b600060208201905081810360008301526110a381611067565b9050919050565b7f4e6f2066756e647320746f207769746864726177000000000000000000000000600082015250565b60006110e060148361102d565b91506110eb826110aa565b602082019050919050565b6000602082019050818103600083015261110f816110d3565b9050919050565b600081905092915050565b50565b6000611131600083611116565b915061113c82611121565b600082019050919050565b600061115282611124565b9150819050919050565b7f4572726f72206f63637572726564207768696c65207472616e7366657272696e60008201527f6720657468000000000000000000000000000000000000000000000000000000602082015250565b60006111b860258361102d565b91506111c38261115c565b604082019050919050565b600060208201905081810360008301526111e7816111ab565b9050919050565b7f43616e6e6f7420627579203020746f6b656e7300000000000000000000000000600082015250565b600061122460138361102d565b915061122f826111ee565b602082019050919050565b6000602082019050818103600083015261125381611217565b9050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601160045260246000fd5b600061129482610ec8565b915061129f83610ec8565b9250817fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff04831182151516156112d8576112d761125a565b5b828202905092915050565b6000815190506112f281610f01565b92915050565b60006020828403121561130e5761130d610efc565b5b600061131c848285016112e3565b91505092915050565b7f4d61726b657420646f6573206e6f74206861766520656e6f75676820746f6b6560008201527f6e73000000000000000000000000000000000000000000000000000000000000602082015250565b600061138160228361102d565b915061138c82611325565b604082019050919050565b600060208201905081810360008301526113b081611374565b9050919050565b60006040820190506113cc6000830185610e9e565b6113d96020830184610ed2565b9392505050565b6113e981610e36565b81146113f457600080fd5b50565b600081519050611406816113e0565b92915050565b60006020828403121561142257611421610efc565b5b6000611430848285016113f7565b91505092915050565b7f4572726f72206f63637572726564207768696c65207472616e7366657272696e60008201527f6720746f6b656e73000000000000000000000000000000000000000000000000602082015250565b600061149560288361102d565b91506114a082611439565b604082019050919050565b600060208201905081810360008301526114c481611488565b9050919050565b7f4e6f7420656e6f75676820746f6b656e7320746f2073656c6c00000000000000600082015250565b600061150160198361102d565b915061150c826114cb565b602082019050919050565b60006020820190508181036000830152611530816114f4565b9050919050565b7f4e487b7100000000000000000000000000000000000000000000000000000000600052601260045260246000fd5b600061157182610ec8565b915061157c83610ec8565b92508261158c5761158b611537565b5b828206905092915050565b60006115a282610ec8565b91506115ad83610ec8565b9250828210156115c0576115bf61125a565b5b828203905092915050565b60006115d682610ec8565b91506115e183610ec8565b9250826115f1576115f0611537565b5b828204905092915050565b7f436f6e747261637420646f6573206e6f74206861766520656e6f75676820657460008201527f6800000000000000000000000000000000000000000000000000000000000000602082015250565b600061165860218361102d565b9150611663826115fc565b604082019050919050565b600060208201905081810360008301526116878161164b565b9050919050565b60006040820190506116a36000830185610e9e565b6116b06020830184610e9e565b9392505050565b7f4e6f7420656e6f75676820616c6c6f77616e636520746f207472616e7366657260008201527f20746f6b656e7300000000000000000000000000000000000000000000000000602082015250565b600061171360278361102d565b915061171e826116b7565b604082019050919050565b6000602082019050818103600083015261174281611706565b9050919050565b600060608201905061175e6000830186610e9e565b61176b6020830185610e9e565b6117786040830184610ed2565b949350505050565b7f4f776e61626c653a206e6577206f776e657220697320746865207a65726f206160008201527f6464726573730000000000000000000000000000000000000000000000000000602082015250565b60006117dc60268361102d565b91506117e782611780565b604082019050919050565b6000602082019050818103600083015261180b816117cf565b905091905056fea2646970667358221220e8c9a021cbb546d268a6725d7ef31583399c07a10bb6df948702e4db233c55ae64736f6c634300080d0033",
}

// MarketABI is the input ABI used to generate the binding from.
// Deprecated: Use MarketMetaData.ABI instead.
var MarketABI = MarketMetaData.ABI

// MarketBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use MarketMetaData.Bin instead.
var MarketBin = MarketMetaData.Bin

// DeployMarket deploys a new Ethereum contract, binding an instance of Market to it.
func DeployMarket(auth *bind.TransactOpts, backend bind.ContractBackend, tokenAddr common.Address) (common.Address, *types.Transaction, *Market, error) {
	parsed, err := MarketMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(MarketBin), backend, tokenAddr)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Market{MarketCaller: MarketCaller{contract: contract}, MarketTransactor: MarketTransactor{contract: contract}, MarketFilterer: MarketFilterer{contract: contract}}, nil
}

// Market is an auto generated Go binding around an Ethereum contract.
type Market struct {
	MarketCaller     // Read-only binding to the contract
	MarketTransactor // Write-only binding to the contract
	MarketFilterer   // Log filterer for contract events
}

// MarketCaller is an auto generated read-only Go binding around an Ethereum contract.
type MarketCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MarketTransactor is an auto generated write-only Go binding around an Ethereum contract.
type MarketTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MarketFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type MarketFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// MarketSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type MarketSession struct {
	Contract     *Market           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// MarketCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type MarketCallerSession struct {
	Contract *MarketCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// MarketTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type MarketTransactorSession struct {
	Contract     *MarketTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// MarketRaw is an auto generated low-level Go binding around an Ethereum contract.
type MarketRaw struct {
	Contract *Market // Generic contract binding to access the raw methods on
}

// MarketCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type MarketCallerRaw struct {
	Contract *MarketCaller // Generic read-only contract binding to access the raw methods on
}

// MarketTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type MarketTransactorRaw struct {
	Contract *MarketTransactor // Generic write-only contract binding to access the raw methods on
}

// NewMarket creates a new instance of Market, bound to a specific deployed contract.
func NewMarket(address common.Address, backend bind.ContractBackend) (*Market, error) {
	contract, err := bindMarket(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Market{MarketCaller: MarketCaller{contract: contract}, MarketTransactor: MarketTransactor{contract: contract}, MarketFilterer: MarketFilterer{contract: contract}}, nil
}

// NewMarketCaller creates a new read-only instance of Market, bound to a specific deployed contract.
func NewMarketCaller(address common.Address, caller bind.ContractCaller) (*MarketCaller, error) {
	contract, err := bindMarket(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &MarketCaller{contract: contract}, nil
}

// NewMarketTransactor creates a new write-only instance of Market, bound to a specific deployed contract.
func NewMarketTransactor(address common.Address, transactor bind.ContractTransactor) (*MarketTransactor, error) {
	contract, err := bindMarket(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &MarketTransactor{contract: contract}, nil
}

// NewMarketFilterer creates a new log filterer instance of Market, bound to a specific deployed contract.
func NewMarketFilterer(address common.Address, filterer bind.ContractFilterer) (*MarketFilterer, error) {
	contract, err := bindMarket(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &MarketFilterer{contract: contract}, nil
}

// bindMarket binds a generic wrapper to an already deployed contract.
func bindMarket(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(MarketABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Market *MarketRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Market.Contract.MarketCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Market *MarketRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Market.Contract.MarketTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Market *MarketRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Market.Contract.MarketTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Market *MarketCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Market.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Market *MarketTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Market.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Market *MarketTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Market.Contract.contract.Transact(opts, method, params...)
}

// Balance is a free data retrieval call binding the contract method 0xb69ef8a8.
//
// Solidity: function balance() view returns(uint256)
func (_Market *MarketCaller) Balance(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Market.contract.Call(opts, &out, "balance")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// Balance is a free data retrieval call binding the contract method 0xb69ef8a8.
//
// Solidity: function balance() view returns(uint256)
func (_Market *MarketSession) Balance() (*big.Int, error) {
	return _Market.Contract.Balance(&_Market.CallOpts)
}

// Balance is a free data retrieval call binding the contract method 0xb69ef8a8.
//
// Solidity: function balance() view returns(uint256)
func (_Market *MarketCallerSession) Balance() (*big.Int, error) {
	return _Market.Contract.Balance(&_Market.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Market *MarketCaller) Owner(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Market.contract.Call(opts, &out, "owner")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Market *MarketSession) Owner() (common.Address, error) {
	return _Market.Contract.Owner(&_Market.CallOpts)
}

// Owner is a free data retrieval call binding the contract method 0x8da5cb5b.
//
// Solidity: function owner() view returns(address)
func (_Market *MarketCallerSession) Owner() (common.Address, error) {
	return _Market.Contract.Owner(&_Market.CallOpts)
}

// Token is a free data retrieval call binding the contract method 0xfc0c546a.
//
// Solidity: function token() view returns(address)
func (_Market *MarketCaller) Token(opts *bind.CallOpts) (common.Address, error) {
	var out []interface{}
	err := _Market.contract.Call(opts, &out, "token")

	if err != nil {
		return *new(common.Address), err
	}

	out0 := *abi.ConvertType(out[0], new(common.Address)).(*common.Address)

	return out0, err

}

// Token is a free data retrieval call binding the contract method 0xfc0c546a.
//
// Solidity: function token() view returns(address)
func (_Market *MarketSession) Token() (common.Address, error) {
	return _Market.Contract.Token(&_Market.CallOpts)
}

// Token is a free data retrieval call binding the contract method 0xfc0c546a.
//
// Solidity: function token() view returns(address)
func (_Market *MarketCallerSession) Token() (common.Address, error) {
	return _Market.Contract.Token(&_Market.CallOpts)
}

// TokensPerEth is a free data retrieval call binding the contract method 0xcbdd69b5.
//
// Solidity: function tokensPerEth() view returns(uint256)
func (_Market *MarketCaller) TokensPerEth(opts *bind.CallOpts) (*big.Int, error) {
	var out []interface{}
	err := _Market.contract.Call(opts, &out, "tokensPerEth")

	if err != nil {
		return *new(*big.Int), err
	}

	out0 := *abi.ConvertType(out[0], new(*big.Int)).(**big.Int)

	return out0, err

}

// TokensPerEth is a free data retrieval call binding the contract method 0xcbdd69b5.
//
// Solidity: function tokensPerEth() view returns(uint256)
func (_Market *MarketSession) TokensPerEth() (*big.Int, error) {
	return _Market.Contract.TokensPerEth(&_Market.CallOpts)
}

// TokensPerEth is a free data retrieval call binding the contract method 0xcbdd69b5.
//
// Solidity: function tokensPerEth() view returns(uint256)
func (_Market *MarketCallerSession) TokensPerEth() (*big.Int, error) {
	return _Market.Contract.TokensPerEth(&_Market.CallOpts)
}

// Buy is a paid mutator transaction binding the contract method 0xa6f2ae3a.
//
// Solidity: function buy() payable returns(bool)
func (_Market *MarketTransactor) Buy(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Market.contract.Transact(opts, "buy")
}

// Buy is a paid mutator transaction binding the contract method 0xa6f2ae3a.
//
// Solidity: function buy() payable returns(bool)
func (_Market *MarketSession) Buy() (*types.Transaction, error) {
	return _Market.Contract.Buy(&_Market.TransactOpts)
}

// Buy is a paid mutator transaction binding the contract method 0xa6f2ae3a.
//
// Solidity: function buy() payable returns(bool)
func (_Market *MarketTransactorSession) Buy() (*types.Transaction, error) {
	return _Market.Contract.Buy(&_Market.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Market *MarketTransactor) RenounceOwnership(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Market.contract.Transact(opts, "renounceOwnership")
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Market *MarketSession) RenounceOwnership() (*types.Transaction, error) {
	return _Market.Contract.RenounceOwnership(&_Market.TransactOpts)
}

// RenounceOwnership is a paid mutator transaction binding the contract method 0x715018a6.
//
// Solidity: function renounceOwnership() returns()
func (_Market *MarketTransactorSession) RenounceOwnership() (*types.Transaction, error) {
	return _Market.Contract.RenounceOwnership(&_Market.TransactOpts)
}

// Sell is a paid mutator transaction binding the contract method 0xe4849b32.
//
// Solidity: function sell(uint256 _tokensToSell) returns(bool)
func (_Market *MarketTransactor) Sell(opts *bind.TransactOpts, _tokensToSell *big.Int) (*types.Transaction, error) {
	return _Market.contract.Transact(opts, "sell", _tokensToSell)
}

// Sell is a paid mutator transaction binding the contract method 0xe4849b32.
//
// Solidity: function sell(uint256 _tokensToSell) returns(bool)
func (_Market *MarketSession) Sell(_tokensToSell *big.Int) (*types.Transaction, error) {
	return _Market.Contract.Sell(&_Market.TransactOpts, _tokensToSell)
}

// Sell is a paid mutator transaction binding the contract method 0xe4849b32.
//
// Solidity: function sell(uint256 _tokensToSell) returns(bool)
func (_Market *MarketTransactorSession) Sell(_tokensToSell *big.Int) (*types.Transaction, error) {
	return _Market.Contract.Sell(&_Market.TransactOpts, _tokensToSell)
}

// SetExchangeRate is a paid mutator transaction binding the contract method 0xdb068e0e.
//
// Solidity: function setExchangeRate(uint256 newTokensPerEth) returns()
func (_Market *MarketTransactor) SetExchangeRate(opts *bind.TransactOpts, newTokensPerEth *big.Int) (*types.Transaction, error) {
	return _Market.contract.Transact(opts, "setExchangeRate", newTokensPerEth)
}

// SetExchangeRate is a paid mutator transaction binding the contract method 0xdb068e0e.
//
// Solidity: function setExchangeRate(uint256 newTokensPerEth) returns()
func (_Market *MarketSession) SetExchangeRate(newTokensPerEth *big.Int) (*types.Transaction, error) {
	return _Market.Contract.SetExchangeRate(&_Market.TransactOpts, newTokensPerEth)
}

// SetExchangeRate is a paid mutator transaction binding the contract method 0xdb068e0e.
//
// Solidity: function setExchangeRate(uint256 newTokensPerEth) returns()
func (_Market *MarketTransactorSession) SetExchangeRate(newTokensPerEth *big.Int) (*types.Transaction, error) {
	return _Market.Contract.SetExchangeRate(&_Market.TransactOpts, newTokensPerEth)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Market *MarketTransactor) TransferOwnership(opts *bind.TransactOpts, newOwner common.Address) (*types.Transaction, error) {
	return _Market.contract.Transact(opts, "transferOwnership", newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Market *MarketSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Market.Contract.TransferOwnership(&_Market.TransactOpts, newOwner)
}

// TransferOwnership is a paid mutator transaction binding the contract method 0xf2fde38b.
//
// Solidity: function transferOwnership(address newOwner) returns()
func (_Market *MarketTransactorSession) TransferOwnership(newOwner common.Address) (*types.Transaction, error) {
	return _Market.Contract.TransferOwnership(&_Market.TransactOpts, newOwner)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns(bool)
func (_Market *MarketTransactor) Withdraw(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Market.contract.Transact(opts, "withdraw")
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns(bool)
func (_Market *MarketSession) Withdraw() (*types.Transaction, error) {
	return _Market.Contract.Withdraw(&_Market.TransactOpts)
}

// Withdraw is a paid mutator transaction binding the contract method 0x3ccfd60b.
//
// Solidity: function withdraw() returns(bool)
func (_Market *MarketTransactorSession) Withdraw() (*types.Transaction, error) {
	return _Market.Contract.Withdraw(&_Market.TransactOpts)
}

// MarketBoughtIterator is returned from FilterBought and is used to iterate over the raw logs and unpacked data for Bought events raised by the Market contract.
type MarketBoughtIterator struct {
	Event *MarketBought // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MarketBoughtIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MarketBought)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MarketBought)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MarketBoughtIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MarketBoughtIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MarketBought represents a Bought event raised by the Market contract.
type MarketBought struct {
	Buyer  common.Address
	Amount *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterBought is a free log retrieval operation binding the contract event 0xc55650ccda1011e1cdc769b1fbf546ebb8c97800b6072b49e06cd560305b1d67.
//
// Solidity: event Bought(address indexed buyer, uint256 amount)
func (_Market *MarketFilterer) FilterBought(opts *bind.FilterOpts, buyer []common.Address) (*MarketBoughtIterator, error) {

	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _Market.contract.FilterLogs(opts, "Bought", buyerRule)
	if err != nil {
		return nil, err
	}
	return &MarketBoughtIterator{contract: _Market.contract, event: "Bought", logs: logs, sub: sub}, nil
}

// WatchBought is a free log subscription operation binding the contract event 0xc55650ccda1011e1cdc769b1fbf546ebb8c97800b6072b49e06cd560305b1d67.
//
// Solidity: event Bought(address indexed buyer, uint256 amount)
func (_Market *MarketFilterer) WatchBought(opts *bind.WatchOpts, sink chan<- *MarketBought, buyer []common.Address) (event.Subscription, error) {

	var buyerRule []interface{}
	for _, buyerItem := range buyer {
		buyerRule = append(buyerRule, buyerItem)
	}

	logs, sub, err := _Market.contract.WatchLogs(opts, "Bought", buyerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MarketBought)
				if err := _Market.contract.UnpackLog(event, "Bought", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseBought is a log parse operation binding the contract event 0xc55650ccda1011e1cdc769b1fbf546ebb8c97800b6072b49e06cd560305b1d67.
//
// Solidity: event Bought(address indexed buyer, uint256 amount)
func (_Market *MarketFilterer) ParseBought(log types.Log) (*MarketBought, error) {
	event := new(MarketBought)
	if err := _Market.contract.UnpackLog(event, "Bought", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// MarketOwnershipTransferredIterator is returned from FilterOwnershipTransferred and is used to iterate over the raw logs and unpacked data for OwnershipTransferred events raised by the Market contract.
type MarketOwnershipTransferredIterator struct {
	Event *MarketOwnershipTransferred // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MarketOwnershipTransferredIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MarketOwnershipTransferred)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MarketOwnershipTransferred)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MarketOwnershipTransferredIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MarketOwnershipTransferredIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MarketOwnershipTransferred represents a OwnershipTransferred event raised by the Market contract.
type MarketOwnershipTransferred struct {
	PreviousOwner common.Address
	NewOwner      common.Address
	Raw           types.Log // Blockchain specific contextual infos
}

// FilterOwnershipTransferred is a free log retrieval operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Market *MarketFilterer) FilterOwnershipTransferred(opts *bind.FilterOpts, previousOwner []common.Address, newOwner []common.Address) (*MarketOwnershipTransferredIterator, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Market.contract.FilterLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return &MarketOwnershipTransferredIterator{contract: _Market.contract, event: "OwnershipTransferred", logs: logs, sub: sub}, nil
}

// WatchOwnershipTransferred is a free log subscription operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Market *MarketFilterer) WatchOwnershipTransferred(opts *bind.WatchOpts, sink chan<- *MarketOwnershipTransferred, previousOwner []common.Address, newOwner []common.Address) (event.Subscription, error) {

	var previousOwnerRule []interface{}
	for _, previousOwnerItem := range previousOwner {
		previousOwnerRule = append(previousOwnerRule, previousOwnerItem)
	}
	var newOwnerRule []interface{}
	for _, newOwnerItem := range newOwner {
		newOwnerRule = append(newOwnerRule, newOwnerItem)
	}

	logs, sub, err := _Market.contract.WatchLogs(opts, "OwnershipTransferred", previousOwnerRule, newOwnerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MarketOwnershipTransferred)
				if err := _Market.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseOwnershipTransferred is a log parse operation binding the contract event 0x8be0079c531659141344cd1fd0a4f28419497f9722a3daafe3b4186f6b6457e0.
//
// Solidity: event OwnershipTransferred(address indexed previousOwner, address indexed newOwner)
func (_Market *MarketFilterer) ParseOwnershipTransferred(log types.Log) (*MarketOwnershipTransferred, error) {
	event := new(MarketOwnershipTransferred)
	if err := _Market.contract.UnpackLog(event, "OwnershipTransferred", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// MarketSoldIterator is returned from FilterSold and is used to iterate over the raw logs and unpacked data for Sold events raised by the Market contract.
type MarketSoldIterator struct {
	Event *MarketSold // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *MarketSoldIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(MarketSold)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(MarketSold)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *MarketSoldIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *MarketSoldIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// MarketSold represents a Sold event raised by the Market contract.
type MarketSold struct {
	Seller common.Address
	Amount *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterSold is a free log retrieval operation binding the contract event 0xae92ab4b6f8f401ead768d3273e6bb937a13e39827d19c6376e8fd4512a05d9a.
//
// Solidity: event Sold(address indexed seller, uint256 amount)
func (_Market *MarketFilterer) FilterSold(opts *bind.FilterOpts, seller []common.Address) (*MarketSoldIterator, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	logs, sub, err := _Market.contract.FilterLogs(opts, "Sold", sellerRule)
	if err != nil {
		return nil, err
	}
	return &MarketSoldIterator{contract: _Market.contract, event: "Sold", logs: logs, sub: sub}, nil
}

// WatchSold is a free log subscription operation binding the contract event 0xae92ab4b6f8f401ead768d3273e6bb937a13e39827d19c6376e8fd4512a05d9a.
//
// Solidity: event Sold(address indexed seller, uint256 amount)
func (_Market *MarketFilterer) WatchSold(opts *bind.WatchOpts, sink chan<- *MarketSold, seller []common.Address) (event.Subscription, error) {

	var sellerRule []interface{}
	for _, sellerItem := range seller {
		sellerRule = append(sellerRule, sellerItem)
	}

	logs, sub, err := _Market.contract.WatchLogs(opts, "Sold", sellerRule)
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(MarketSold)
				if err := _Market.contract.UnpackLog(event, "Sold", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSold is a log parse operation binding the contract event 0xae92ab4b6f8f401ead768d3273e6bb937a13e39827d19c6376e8fd4512a05d9a.
//
// Solidity: event Sold(address indexed seller, uint256 amount)
func (_Market *MarketFilterer) ParseSold(log types.Log) (*MarketSold, error) {
	event := new(MarketSold)
	if err := _Market.contract.UnpackLog(event, "Sold", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
