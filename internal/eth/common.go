package eth

import (
	"crypto/ecdsa"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/pkg/errors"
)

func IsZeroAddress(address string) bool {
	return address == common.Address{}.String()
}

func AddressFromPrivateKey(pvk string) (common.Address, error) {
	pvkECDSA, err := crypto.HexToECDSA(pvk)
	if err != nil {
		return common.Address{}, errors.Wrap(err, "failed to convert to ECDSA")
	}

	pubECDSA, ok := pvkECDSA.Public().(*ecdsa.PublicKey)
	if !ok {
		return common.Address{}, errors.Wrap(err, "failed to convert to pub key")
	}

	return crypto.PubkeyToAddress(*pubECDSA), nil
}
