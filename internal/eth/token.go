package eth

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"gitlab.com/dovgopoly/market-contract/internal/generated"
	"math/big"
)

func NewTokenContract(address string, client *ethclient.Client) (*TokenContract, error) {
	token, err := generated.NewToken(common.HexToAddress(address), client)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get instance of token contract")
	}
	return &TokenContract{
		client:       client,
		tokenBackend: token,
	}, nil
}

type TokenContract struct {
	client       *ethclient.Client
	tokenBackend *generated.Token
}

func (c *TokenContract) GetMarketAddress(name string) (string, error) {
	address, err := c.tokenBackend.TokenCaller.Markets(nil, name)
	if err != nil {
		return "", errors.Wrap(err, "failed to get market address")
	}
	return address.String(), nil
}

func (c *TokenContract) AddMarket(name string, initBalance *big.Int, ownerPvk string) error {
	opts, err := NewOptsBuilder(c.client).WithFrom(ownerPvk).Build()
	if err != nil {
		return errors.Wrap(err, "failed to get opts")
	}

	if _, err := c.tokenBackend.TokenTransactor.AddMarket(opts, name, initBalance); err != nil {
		return errors.Wrap(err, "failed to add market")
	}

	return nil
}

func (c *TokenContract) GetMarket(name string) (*MarketContract, error) {
	address, err := c.GetMarketAddress(name)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get market address")
	}

	if IsZeroAddress(address) {
		return nil, errors.New("market does not exist")
	}

	market, err := NewMarketContract(address, c.client)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create market contract")
	}

	return market, nil
}

func (c *TokenContract) BalanceOf(address common.Address) (*big.Int, error) {
	balance, err := c.tokenBackend.TokenCaller.BalanceOf(nil, address)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get balance")
	}

	return balance, nil
}

func (c *TokenContract) Approve(spender common.Address, amount *big.Int, clientPvk string) error {
	opts, err := NewOptsBuilder(c.client).WithFrom(clientPvk).Build()
	if err != nil {
		return errors.Wrap(err, "failed to get opts")
	}

	if _, err := c.tokenBackend.TokenTransactor.Approve(opts, spender, amount); err != nil {
		return errors.Wrap(err, "failed to approve")
	}

	return nil
}
