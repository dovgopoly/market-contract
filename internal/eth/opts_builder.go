package eth

import (
	"context"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"math/big"
)

type OptsBuilder struct {
	client  *ethclient.Client
	fromPvk string
	value   *big.Int
}

func NewOptsBuilder(client *ethclient.Client) *OptsBuilder {
	return &OptsBuilder{
		client: client,
	}
}

func (b *OptsBuilder) WithFrom(fromPvk string) *OptsBuilder {
	b.fromPvk = fromPvk
	return b
}

func (b *OptsBuilder) WithValue(value *big.Int) *OptsBuilder {
	b.value = value
	return b
}

func (b *OptsBuilder) Build() (*bind.TransactOpts, error) {
	if len(b.fromPvk) == 0 {
		return nil, nil
	}

	chainID, err := b.client.ChainID(context.Background())
	if err != nil {
		return nil, errors.Wrap(err, "failed to get chain id")
	}

	ownerPvkECDSA, err := crypto.HexToECDSA(b.fromPvk)
	if err != nil {
		return nil, errors.Wrap(err, "wrong private key provided")
	}

	opts, err := bind.NewKeyedTransactorWithChainID(ownerPvkECDSA, chainID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get opts")
	}

	opts.Value = b.value

	return opts, nil
}
