package eth

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/pkg/errors"
	"gitlab.com/dovgopoly/market-contract/internal/generated"
	"math/big"
)

type MarketContract struct {
	client        *ethclient.Client
	marketBackend *generated.Market
}

func NewMarketContract(address string, client *ethclient.Client) (*MarketContract, error) {
	market, err := generated.NewMarket(common.HexToAddress(address), client)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get instance of market contract")
	}
	return &MarketContract{
		client:        client,
		marketBackend: market,
	}, nil
}

func (c *MarketContract) Buy(ethToTransfer *big.Int, customerPvk string) error {
	opts, err := NewOptsBuilder(c.client).WithFrom(customerPvk).WithValue(ethToTransfer).Build()
	if err != nil {
		return errors.Wrap(err, "failed to get opts")
	}

	_, err = c.marketBackend.MarketTransactor.Buy(opts)
	if err != nil {
		return errors.Wrap(err, "failed to buy tokens")
	}

	return nil
}

func (c *MarketContract) Sell(tokensToSell *big.Int, customerPvk string) error {
	opts, err := NewOptsBuilder(c.client).WithFrom(customerPvk).Build()
	if err != nil {
		return errors.Wrap(err, "failed to get opts")
	}

	_, err = c.marketBackend.MarketTransactor.Sell(opts, tokensToSell)
	if err != nil {
		return errors.Wrap(err, "failed to sell tokens")
	}

	return nil
}
